from typing import List, Tuple
import pprint
from itertools import combinations
import subprocess
import test

# aliases de type
Variable = int
Literal = int
Clause = List[Literal]
Model = List[Literal]
Clause_Base = List[Clause]
Grid = List[List[int]]

example: Grid = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],
    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],
    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9],
]


example2: Grid = [
    [0, 0, 0, 0, 2, 7, 5, 8, 0],
    [1, 0, 0, 0, 0, 0, 0, 4, 6],
    [0, 0, 0, 0, 0, 9, 0, 0, 0],
    [0, 0, 3, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 5, 0, 2, 0],
    [0, 0, 0, 8, 1, 0, 0, 0, 0],
    [4, 0, 6, 3, 0, 1, 0, 0, 9],
    [8, 0, 0, 0, 0, 0, 0, 0, 0],
    [7, 2, 0, 0, 0, 0, 3, 1, 0],
]


empty_grid: Grid = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
]

# ==================================================================================
# =============================== Fonctions  fournies ==============================
# ==================================================================================


def write_dimacs_file(dimacs: str, filename: str):
    with open(filename, "w", newline="") as cnf:
        cnf.write(dimacs)


def exec_gophersat(
    filename: str, cmd: str = "./gophersat", encoding: str = "utf8"
) -> Tuple[bool, List[int]]:
    result = subprocess.run(
        [cmd, filename], capture_output=True, check=True, encoding=encoding
    )
    string = str(result.stdout)
    lines = string.splitlines()

    if lines[1] != "s SATISFIABLE":
        return False, []

    model = lines[2][2:].split(" ")

    return True, [int(x) for x in model]


# ==================================================================================
# =============================== Fonctions non fournies ===========================
# ==================================================================================


def cell_to_variable(i: int, j: int, val: int) -> int:
    return i * 81 + j * 9 + val


def variable_to_cell(lit: int) -> Tuple[int, int, int]:
    i = lit
    if i % 9 == 0:
        i -= 1
    i = i // 81
    j = lit - i * 81
    if j % 9 == 0:
        j -= 1
    j = j // 9
    val = lit - i * 81 - j * 9
    return (i, j, val)


def at_least_one(vars: List[int]) -> List[int]:
    result = vars
    return result


def unique(vars: List[int]) -> List[List[int]]:
    result = []
    result.append(vars)
    comb = combinations(vars, 2)
    for c in comb:
        list_combinations = list(c)
        for i in range(2):
            list_combinations[i] *= -1
        result.append(list_combinations)
    return result


def create_cell_constraints() -> List[List[int]]:
    result: List[List[int]] = []
    # looping through each spot
    for i in range(9):
        for j in range(9):
            variableListForOneSpot = []
            for val in range(1, 10):
                variableListForOneSpot.append(cell_to_variable(i, j, val))
            result += unique(variableListForOneSpot)
    return result


def create_line_constraints() -> List[List[int]]:
    result = []
    for value in range(1, 10):  # looping through each possible value for a spot
        for i in range(9):  # looping through each line
            variableListForOneLine = []
            for j in range(9):  # looping through each column in that line
                variableListForOneLine.append(cell_to_variable(i, j, value))
            result.append(at_least_one(variableListForOneLine))
    return result


def create_column_constraints() -> List[List[int]]:
    result = []
    for value in range(1, 10):  # looping through each possible value for a spot
        for j in range(9):  # looping through each column
            variableListForOneColumn = []
            for i in range(9):  # looping through each line in that column
                variableListForOneColumn.append(cell_to_variable(i, j, value))
            result.append(at_least_one(variableListForOneColumn))
    return result


def create_box_constraints() -> List[List[int]]:
    result = []
    for val in range(1, 10):
        for boxLine in range(3):
            for boxColumn in range(3):
                variableListForOneBox = []
                for i in range(boxLine * 3, boxLine * 3 + 3):
                    for j in range(boxColumn * 3, boxColumn * 3 + 3):
                        variableListForOneBox.append(cell_to_variable(i, j, val))
                result.append(at_least_one(variableListForOneBox))
    return result


def create_value_constraints(grid: List[List[int]]) -> List[List[int]]:
    result = []
    for i in range(9):
        for j in range(9):
            if grid[i][j] != 0:
                result.append([cell_to_variable(i, j, grid[i][j])])
    return result


def generate_problem(grid: List[List[int]]) -> List[List[int]]:
    result: List[List[int]] = []
    result += create_cell_constraints()
    result += create_line_constraints()
    result += create_column_constraints()
    result += create_box_constraints()
    result += create_value_constraints(grid)
    return result


def clauses_to_dimacs(clauses: List[List[int]], nb_vars: int) -> str:
    dimacs = ""
    dimacs += "p cnf " + str(nb_vars) + " " + str(len(clauses)) + "\n"
    for clause in clauses:
        for variable in clause:
            dimacs += str(variable) + " "
        dimacs += "0\n"
    return dimacs


def model_to_grid(model: List[int], nb_vals: int = 9) -> List[List]:
    grid = []
    for i in range(9):
        line = []
        for j in range(9):
            line.append(0)
        grid.append(line)
    for i in range(len(model)):
        variable = model[i]
        if variable > 0:
            coord = variable_to_cell(variable)
            grid[coord[0]][coord[1]] = coord[2]
    return grid

def print_grid(grid: List[List[int]]):
    lineGrid = "-------------------------"
    for i in range(9):
        if i!=0:
            print("|")
        if i%3==0:
            print(lineGrid)
        for j in range(9):
            end = ""
            if j%3==2:
                end = " "
            if j%3==0:
                print("|", end="")
            value = str(grid[i][j])
            if grid[i][j] == 0:
                value = "."
            print(" " + value, end=end)

    print("|")
    print(lineGrid)



# ==================================================================================
# =============================== Main ============================================
# ==================================================================================


def main():
    pp = pprint.PrettyPrinter()

    constraints = generate_problem(example)
    write_dimacs_file(clauses_to_dimacs(constraints, 729), "example.cnf")
    solution = exec_gophersat("example.cnf")

    print()
    print("Before :")
    print_grid(example)
    print("\nAfter :")
    print_grid(model_to_grid(solution[1]))

    print()

    constraints = generate_problem(example2)
    write_dimacs_file(clauses_to_dimacs(constraints, 729), "example2.cnf")
    solution = exec_gophersat("example2.cnf")

    print()
    print("Before :")
    print_grid(example2)
    print("\nAfter :")
    print_grid(model_to_grid(solution[1]))


if __name__ == "__main__":
    main()
