# Sudoku Solver

Solveur de sudoku réalisé avec python3.
Le programme nécessite le solveur SAT _gophersat_ installé avec l'éxécutable dans le répertoire de l'application. Voir https://github.com/crillab/gophersat
